'use strict';

const browserSync = require('browser-sync').create(),
      gulp        = require('gulp'),
      plugins     = require('gulp-load-plugins')(),
      uglifyES    = require('uglify-es'),
      composer    = require('gulp-uglify/composer');

plugins.fs                      = require('fs');
plugins.path                    = require('path');
plugins.merge                   = require('merge-stream');
plugins.imageminJpegRecompress  = require('imagemin-jpeg-recompress');
plugins.pngquant                = require('imagemin-pngquant');
plugins.syncy                   = require('syncy');
plugins.uglifyES                = composer(uglifyES, console);

plugins.repl = require('./repl');


gulp.task('pl', function () {
  console.log(plugins);
});

/*
gulp-load-plugins:
  (+)autoprefixer
  (+)changed
  (+)changedInPlace
  (+)clean
  (+)combineMq
  (+)concat
  (+)csso
  (+)debug
  (+)fileInclude
  (+)htmlBeautify
  (+)htmlmin
  (+)if
  (+)imagemin
  (+)plumber
  (+)rename
  (+)sass
  (+)sequence
  (+)sourcemaps
  (+)uglify
  (+)watch
  (+)spritesmith
  (+)imagemin-jpeg-recompress
  (+)imagemin-pngquant
  (+)fs
  (+)path
  (+)merge
  (+)repl
*/

/* Secondary functions */
let getFolders = function (dir) {
  return plugins.fs.readdirSync(dir)
    .filter(function (file) {
      return plugins.fs.statSync(plugins.path.join(dir, file)).isDirectory();
    });
};
/* End Secondary functions */

/* Paths and variables */
let src = {
    main: 'src',
    scss: 'src/scss',
    js: 'src/js',
    components: 'src/components',
    templates: 'src/templates',
    img: 'src/img'
  },
  dist = {
    main: 'dist',
    css: 'dist/css',
    js: 'dist/js',
    img: 'dist/img'
  };
/* End Paths and variables */

/* Service tasks */
gulp.task('clean-css-maps', function () {
  return gulp.src([dist.css + '/maps', dist.css + '/components/maps'], {read: false})
    .pipe(plugins.clean())
});

gulp.task('img-sync', function (done) {
  plugins.syncy([src.img + '/**/*.{jpg,jpeg,png,gif}', '!src/img/sprite/*.*'], dist.img).then(() => {done()}).catch((err) => {done(err)});
});
/* End Service tasks */


gulp.task('scss', require('./gulpfile_css')(gulp, plugins, getFolders, false));
gulp.task('scss:release', require('./gulpfile_css')(gulp, plugins, getFolders, true));
gulp.task('js', require('./gulpfile_js')(gulp, plugins, getFolders));
gulp.task('html', require('./gulpfile_html')(gulp, plugins, getFolders));
gulp.task('vendor', require('./gulpfile_vendor')(gulp, plugins));
gulp.task('img', require('./gulpfile_img')(gulp, plugins));
gulp.task('sprite', require('./gulpfile_sprite')(gulp, plugins));

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: dist.main
    },
    port: 4000
  });

  browserSync.watch(dist.main + '/*.html').on('change', browserSync.reload);
  browserSync.watch([dist.css + '/*.css', dist.css + '/components/*.css'], function (event, file) {
    if (event === 'change') browserSync.reload();
  });
  browserSync.watch([dist.js + '/*.js', dist.js + '/components/*.js'], function (event, file) {
    if (event === 'change') browserSync.reload();
  });
});

gulp.task('watch', function () {
  gulp.watch([src.scss + '/**/*.scss', src.components + '/**/*.scss'], {usePolling: true, cwd:'./'}, ['scss']);
  gulp.watch([src.js + '/**/*.js', src.components + '/**/*.js'], {cwd:'./'}, ['js']);
  gulp.watch([src.components + '/**/*.html', src.templates + '/**/*.html'], {cwd:'./'}, ['html']);
  gulp.watch([src.img + '/**/*.{jpg,jpeg,png,gif}', '!src/img/sprite/*.*'], {cwd:'./'}, ['img']);
  gulp.watch(src.img + '/sprite/*.*', {cwd:'./'}, ['sprite']);
});

gulp.task('default', plugins.sequence('vendor', 'scss', 'js', 'html', 'img', 'sprite', ['browser-sync', 'watch']));
gulp.task('release', plugins.sequence('vendor', 'clean-css-maps', 'scss:release', 'js', 'html', 'img', 'sprite'));
