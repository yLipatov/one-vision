module.exports = function (gulp, plugins) {
  function onError(e) {
    console.log(e.toString());
    this.emit('end');
  }

  /* Paths */
  let src = {
      main: ['src/img/sprite/*.*', '!src/img/sprite/spritesmith.scsstemplate'],
      scssSprite: 'src/scss',
      cssTemplate: 'src/img/sprite/spritesmith.scsstemplate'
    },
    dist = {
      main: 'dist/img',
      imgSpritePath: 'dist/img/sprite.png'
    };
  /* End Paths */

  return function () {

    let spriteData = gulp.src(src.main)
      .pipe(plugins.plumber({errorHandler: onError}))
      .pipe(plugins.spritesmith({
        imgName: 'sprite.png',
        imgPath: dist.imgSpritePath,
        cssTemplate: src.cssTemplate,
        cssName: '_sprite.scss',
        algorithm: 'binary-tree', //top-down
        cssFormat: 'scss_maps',
        padding: 20,
      }));

    spriteData.img.pipe(gulp.dest(dist.main));
    return spriteData.css.pipe(gulp.dest(src.scssSprite));

  };
};