(function ($) {
  "use strict";

  /*$.fn.Coordinates = function(){

    let rect = this.get(0).getBoundingClientRect();

    return {
      top: rect.top,
      right: document.documentElement.clientWidth - rect.right,
      bottom: document.documentElement.clientHeight - rect.bottom,
      left: rect.left,
      width: rect.width,
      height: rect.height
    }

  };*/



  let Coordinates = function (el) {
    let element =  document.querySelector(el);

    if (typeof(element) !== undefined && element !== null) {
      let rect = element.getBoundingClientRect();

      return {
        top: rect.top,
        right: document.documentElement.clientWidth - rect.right,
        bottom: document.documentElement.clientHeight - rect.bottom,
        left: rect.left,
        width: rect.width,
        height: rect.height
      }
    }
  };


  console.log(Coordinates('.test'));


})(jQuery);